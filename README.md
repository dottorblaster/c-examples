# C Examples

Some C exercises related to (but not only) my OSes exam.

These are essentially split in two subdirs:

- _my-own_: own stuff, like reimplementations of nice things
- _tlpi_: exercises taken from The Linux Programming Interface

